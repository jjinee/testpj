package com.yedam.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages="com.yedam.app.**.mapper") 
//매퍼를 하나씩 등록하는게 아닌 페키지 경로를 지정하여 
//이하 위치에있는 인터페이스들은 전부 맵퍼로 사용
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
