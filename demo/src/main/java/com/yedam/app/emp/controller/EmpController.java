package com.yedam.app.emp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yedam.app.emp.service.EmpService;
import com.yedam.app.emp.service.EmpVO;

@Controller
public class EmpController {
	
	@Autowired
	EmpService empService;
	
	@RequestMapping(value="/empList", method=RequestMethod.GET) // 경로 매핑
	// = @GetMapping("empList")
	public String empAllList(Model model) {
		// 데이터 뿌려주기 위해 model에 데이터 담음
		// model : 생선된 데이터를 JSP 즉 View에 전달
		model.addAttribute("empList", empService.getAllList()); 
		// 서비스에서 작업한 데이터를 모델에 담고 empList에 전달
		return "empList";
	}
	
	
	@GetMapping("/empInsert") // 단순 페이지 불러오는 거
	public String inputEmpForm() {
		return "empInsert";
	}
	
	@PostMapping("/empInsert") // 데이터 받아서 등록하는 거
	public String inputEmpProcess(EmpVO empVO) { // 비동기통신기반 처리
		empService.insertEmpInfo(empVO);
		return "redirect:empList"; // 해당경로(컨트롤러) 호출
	}
}
