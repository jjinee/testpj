package com.yedam.app.emp.mapper;

import java.util.List;

import com.yedam.app.emp.service.EmpVO;

public interface EmpMapper {
	// 전체조회
	public List<EmpVO> selectAllEmpList();
	
	//등록
	public int insertEmpinfo(EmpVO empVO);
	
	// Rest 단건조회
	public EmpVO selectEmpInfo(int empId);

}
