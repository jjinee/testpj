package com.yedam.app.emp.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class EmpVO {
	int employeeId;
	String firstName;
	String lastName;
	String email;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	// @DateTimeFormat 값에 대한 양식 지정
	// 자바 기본 date 포맷 yyyy/mm/dd
	Date hireDate;
	String jobId;
	int departmentId;
}
